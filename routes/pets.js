const express = require("express");
const router = express.Router();
const db = require("../db");

router.get("/", async (req, res) => {
  // console.log("Request captured is: ", req.body);
  // console.log("Request Header captured is: ", req.header);
  const data = await db.query("SELECT * FROM pets");
  return res.json(data.rows);
});

router.post("/", async (req, res) => {
  const data = await db.query(
    "INSERT INTO pets (name) VALUES ($1) RETURNING *",
    [req.body.name]
  );
  // req.pipe('RequestCaptured.txt');
  // console.log('Request for POST: ', req);
  return res.json(data.rows[0]);
});

router.patch("/:id", async (req, res) => {
  const data = await db.query(
    "UPDATE pets SET name=$1 WHERE id=$2 RETURNING *",
    [req.body.name, req.params.id]
  );
  return res.json(data.rows[0]);
});

router.delete("/:id", async (req, res) => {
  const data = await db.query("DELETE FROM pets WHERE id=$1", [
    req.params.id
  ]);
  return res.json({ message: "Deleted" });
});

module.exports = router;
