const express = require("express");
const router = express.Router();
const db = require("../db");

router.get("/status", async (req, res) => {
  const data = await db.query("SELECT * FROM statuscode");
  return res.json(data.rows);
});

router.post("/statusCode", async (req, res) => {
  const data = await db.query(
    "INSERT INTO statuscode (status,description) VALUES ($1,$2) RETURNING *",
    [req.params.status, req.body.description]
  );
  return res.json(data.rows[0]);
});

router.patch("/:status", async (req, res) => {
  const data = await db.query(
    "UPDATE statuscode SET description=$2 WHERE status=$1 RETURNING *",
    [req.body.description, req.params.status]
  );
  return res.json(data.rows[0]);
});

router.delete("/:status", async (req, res) => {
  const data = await db.query("DELETE FROM statuscode WHERE status=$1", [
    req.params.status
  ]);
  return res.json({ message: "Deleted" });
});

module.exports = router;
