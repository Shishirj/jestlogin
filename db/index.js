const { Client } = require("pg");
const db = "pets";

const client = new Client({
  connectionString: `postgresql://postgres:360logica@127.0.0.1/${db}`
});

client.connect();

module.exports = client;
