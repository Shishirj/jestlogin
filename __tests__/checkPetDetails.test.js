// import request from 'supertest';
// import app from 'express';
var config = require('../Data/apiconfig.json');
var postData = require('../Data/petPostData.json');
const request = require('supertest');

let generatedRandomNumber;
var postData;


describe('Create a Pet, Get Details and Delete it', () => {

  async function generateRandom() {
    var randomNumber = Math.floor(Math.random() * config.randomNumberRange) + 100 ;
    console.log('Random Number generated is '+randomNumber) ;
    return randomNumber;
  }
  
  beforeAll(async  () => {

    generatedRandomNumber = await generateRandom();
    postData.id=generatedRandomNumber;
    postData.name=config.petName;

  });
  
  afterAll(() => {
    //Close Server and Printout the report
  })

    

    test('Creata a Pet with random ID', async () => {
        const response  = await request(config.baseURL)
                            .post('')
                            .send(postData)
                            .set('accept', 'application/json')
                            .set('Content-Type', 'application/json');
        expect(response.status).toBe(config.passCode);
        // console.log(response.body);
    });

    test('Get Pet Details', async () =>{
        const response  = await request(config.baseURL)
                            .get('/'+generatedRandomNumber)
                            .set('accept', 'application/json')
                            .set('api_key','Shishirj');
        
        expect(response.status).toBe(config.passCode);
        expect(response.body.name).toBe(config.petName);
        console.log(response.body);
    });

    test('Delete Pet using ID', async () =>{
        const response  = await request(config.baseURL)
                            .delete('/'+generatedRandomNumber)
                            .set('accept', 'application/json')
                            .set('api_key','Shishirj');
        expect(response.status).toBe(config.passCode);
        console.log(response.body);
    });

    test('Check Deleted Pet', async () =>{
        const response  = await request(config.baseURL)
                            .get('/'+generatedRandomNumber)
                            .set('accept', 'application/json')
                            .set('api_key','Shishirj');
        expect(response.status).toBe(config.failCode);
        // expect(response.body.name).toBe('Dog');
        // console.log(response.body);
    });
});