import puppeteer from 'puppeteer';
// import webdriver from 'selenium-webdriver';
// import chrome from 'selenium-webdriver/chrome';
// import chromedriver from 'chromedriver';
import LoginPageClass from '../PageClasses/LoginPageClass';
import DashboardPageClass from '../PageClasses/DashboardPageClass';

let loginPO = new LoginPageClass();
let dashboardPO;
let userName = process.argv[3];
let passwd = process.argv[4];

if(userName==null || userName === 'undefined' )
{
  userName = 'shishir19@gmail.com';
}

if(passwd==null || passwd === 'undefined' )
{
  passwd = 'Test@1234';
}

console.log(userName);
console.log(passwd);

describe('Deutsche Bank Dev Login', () => {

  let browser;
  let page;
  // chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());

  // beforeAll(async () => {
  //   browser = await puppeteer.launch({headless:false, slowMo:50, args: ['--start-maximized']});
  //   page = await browser.newPage();
  //   await page.setViewport({ width: 1366, height: 768});
  //   await page.goto('https://developer.db.com/');
  // });

  // afterAll(async () => {
  //   await browser.close();
  // });

  test('Login to Deutsche Bank with Page Object', async () =>{

    //Initialize Browser and Page
    await loginPO.init();

    //Set Window Size and Timeout
    await loginPO.setScreenSizeAndTimeout();

    //Launch the application
    await loginPO.open('https://developer.db.com/');

    //Click Log in Link
    await loginPO.clickLoginLink("Error clicking Login Link");

    //Enter Username
    await loginPO.inputUserName(userName);
  
    //Enter Password
    await loginPO.inputPassWord(passwd);

    //Click Form Submit Button
    await loginPO.clickFormSubmit("Error clicking Form Submit button");

    //Verify Dashboard Page is Displayed on Successful Login
    await dashboardPO.verifyDashboardPage();

    //Close Browser
    await dashboardPO.close();

  },300000);

  // test('Check WebDriver',async () => {

  //   var chromeCapabilities = webdriver.Capabilities.chrome();
  //   var chromeOptions = {'args':['--test-type', '--start-maximized']};
  //   chromeCapabilities.set('chromeOptions',chromeOptions);

  //   let driver = new webdriver.Builder()
  //                .withCapabilities(chromeCapabilities)
  //                .build();
  //   try {
  //     await driver.get('https://developer.db.com/');
  //     await driver.wait(webdriver.until.elementIsVisible(webdriver.By.xpath("//span[contains(@class,'log-in-opener')]/a")),5000);
  //     await driver.findElement(webdriver.By.xpath("//span[contains(@class,'log-in-opener')]/a")).click();
  //     // await driver.findElement(webdriver.By.name('q')).sendKeys('webdriver', webdriver.Key.RETURN);
  //     // await driver.wait(webdriver.until.titleIs('webdriver - Google Search'), 1000);
  //   } finally {
  //     await driver.quit();
  //   }
  // },100000)

});

