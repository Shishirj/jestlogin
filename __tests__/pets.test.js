const db = require("../db");
const request = require("supertest");
const app = require("../app");
const CSVUtil = require('../config/CSVUtil');
const csvUtil = new CSVUtil();

let petsArr = new Array();
var statusCode;

beforeAll(async () => {

  await db.query("CREATE TABLE pets (id SERIAL PRIMARY KEY, name TEXT)");
  petsArr = await csvUtil.returnDataArray();
  statusCode = await csvUtil.readStatusCode();
  
});

beforeEach(async () => {
  // seed with some data
  for(var i=0;i<petsArr.length;i++)
  {
    await db.query('INSERT INTO pets (name) VALUES ($1)',[petsArr[i]]);
  }
  
});

afterEach(async () => {
  await db.query("DELETE FROM pets");
});

afterAll(async () => {
  await db.query("DROP TABLE pets");
  db.end();
});

// describe("GET / ", () => {
//   test("It should respond with an array of pets", async () => {
//     const response = await request(app).get("/");
//     expect(response.body).toEqual(["Cat","Dog","Rat","Monkey"]);
//     expect(response.statusCode).toBe(200);
//   });
// });

describe("GET /pets", () => {
  test("It should respond with an array of pets", async () => {
    const response = await request(app).get("/pets");
    console.log(response.body);
    expect(response.body.length).toBe(petsArr.length);
    expect(response.statusCode.toString()).toEqual(statusCode);
    for(var i=0;i<petsArr.length;i++)
    {
        expect(response.body[i]).toHaveProperty("id");
        expect(response.body[i]).toHaveProperty("name");    
        expect(response.body[i].name).toEqual(petsArr[i]);
    }
   
  });
});

describe("POST /pets", () => {
  test("POST a New Pet and check its been inserted into database", async () => {
    const newPetName = 'Camel';
    const newPet = await request(app)
      .post("/pets")
      .send({
        name: newPetName
      });
    expect(newPet.body.name).toBe(newPetName);
    expect(newPet.body).toHaveProperty("id");
    expect(newPet.body).toHaveProperty("name");
    expect(newPet.statusCode.toString()).toBe(statusCode);

    //Add New Pet to Array and check the complete Array against Get Response
    petsArr.push(newPetName);
    // make sure we have 3 pets
    const response = await request(app).get("/pets");
    expect(response.body.length).toBe(petsArr.length);
    for(var i=0;i<petsArr.length;i++)
    {  
        expect(response.body[i].name).toEqual(petsArr[i]);
    }
  });
});

describe("PATCH /pets/1", () => {
  test("PATCH test - Post a new pet and update it", async () => {
    let newPetName = 'Tigor';
    const newPet = await request(app)
      .post("/pets")
      .send({
        name: newPetName
      });

    newPetName = 'Tiger';
    const updatedPet = await request(app)
      .patch(`/pets/${newPet.body.id}`)
      .send({ name: newPetName });
    expect(updatedPet.body.name).toBe(newPetName);
    expect(updatedPet.body).toHaveProperty("id");
    expect(updatedPet.body).toHaveProperty("name");
    expect(updatedPet.statusCode.toString()).toBe(statusCode);

    //Add new pet to Array
    petsArr.push(newPetName);
    const response = await request(app).get("/pets");
    expect(response.body.length).toBe(petsArr.length);
    for(var i=0;i<petsArr.length;i++)
    {  
        expect(response.body[i].name).toEqual(petsArr[i]);
    }
  });
});

describe("DELETE /pets/1", () => {
  test("Add a New Pet and Delete it", async () => {
    let newPetName = 'Fox';
    const newPet = await request(app)
      .post("/pets")
      .send({
        name: newPetName
      });
    const removedPet = await request(app).delete(
      `/pets/${newPet.body.id}`
    );
    expect(removedPet.body).toEqual({ message: "Deleted" });
    expect(removedPet.statusCode.toString()).toBe(statusCode);

    // make sure we still have 2 pets
    const response = await request(app).get("/pets");
    expect(response.body.length).toBe(petsArr.length);
  });
});

describe("Test a 404", () => {
  test("It should respond with a 404 status", async () => {
    const response = await request(app).get("/nowhere");
    expect(response.statusCode).toBe(404);
  });
});