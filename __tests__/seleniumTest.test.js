var webdriver = require('selenium-webdriver'),
chromeDriver = require('selenium-webdriver/chrome'),
By = require('selenium-webdriver').By,
until = require('selenium-webdriver').until;

describe('Test DB', () => {

    var driver;
    const TIMEOUT = 30000;
    
    beforeAll(async () => {
        var options = new chromeDriver.Options();
        options.addArguments('start-maximized');
        options.addArguments('incognito');
        // options.setUserPreferences({'download.default_directory' : '/path/to/your/download/directory'});
        
        driver = new webdriver.Builder()
            .withCapabilities(webdriver.Capabilities.chrome())
            .forBrowser('chrome')
            .setChromeOptions(options)
            .build();
        
        await driver.manage().setTimeouts( { implicit: TIMEOUT, pageLoad: TIMEOUT, script: TIMEOUT } )
        console.info( await driver.manage().getTimeouts() );
    });

    afterAll(async () => {
        driver.quit();
    });

    test('Open DB Website', async () => {

		await driver.get('https://developer.db.com/');
        var loginLink = await driver.findElement(By.css('a.no-underline.cursor'));
        await loginLink.click();

        var userName = await driver.wait(until.elementIsVisible(await driver.findElement(By.css('#mat-input-1')),4000));
        await userName.sendKeys("shishir19@gmail.com");

        var passWord = await driver.wait(until.elementIsVisible(await driver.findElement(By.css('#mat-input-2')),4000));
        await passWord.sendKeys("Test@1234");

        var formSubmit = await driver.findElement(By.css('.btn-plain + .mat-button > span.mat-button-wrapper'),5000);
        await formSubmit.click();

        var dashboardNav = await driver.wait(until.elementIsVisible(await driver.findElement(By.css('.side-nav'),10000)));
        expect(dashboardNav).toBeTruthy();

    });

});