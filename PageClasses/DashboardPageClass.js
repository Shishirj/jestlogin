import CommonUtils from '../Utils/CommonUtils';
import BasePage from './BasePage';
import PageObject from 'puppeteer-page-object';

const basePage =  new BasePage();
let commonUtils = new CommonUtils();

class DashboardPageClass extends PageObject {
    constructor(page){
        super();
        this.page = page
        
    }

    async waitForSelector(selector) {
        return await basePage.waitForSelector(selector);
      }
}

module.exports=DashboardPageClass;