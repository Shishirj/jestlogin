import CommonUtils from '../Utils/CommonUtils';
import BasePage from './BasePage';
import PageObject from 'puppeteer-page-object';
import DashboardPageClass from './DashboardPageClass';

let commonUtils = new CommonUtils();
const basePage = new BasePage();

const loginLink = 'a.no-underline.cursor';
const formSubmitBtn = '.btn-plain + .mat-button > span.mat-button-wrapper';
const userNameInput = '#mat-input-1';
const passWordInput = '#mat-input-2';
const browserTimeout = 60000;

class LoginPageClass extends PageObject {

    constructor(){
        super({headless:false,slowMo:50,args: ['--start-maximized']});
    }

    async setScreenSizeAndTimeout() {
        basePage.initPage(this.page);
        await commonUtils.optimizeViewPort(this.page);
        await commonUtils.defaultTimeOut(this.page,browserTimeout);
    }

    async waitForLoginLink() {
        await basePage.waitForSelector(loginLink);
    }

    async clickLoginLink(message) {
        await this.waitForLoginLink(loginLink);
        await basePage.clickElement(loginLink,message);
    }

    async clickFormSubmit(message) {
        await basePage.clickElement(formSubmitBtn,message);
        return new DashboardPageClass(this.page);
    }
  
    async inputUserName(text) {
        await basePage.waitForSelector(userNameInput);
        await basePage.enterText(userNameInput,text);
    }

    async inputPassWord(text) {
        await basePage.waitForSelector(passWordInput);
        await basePage.enterText(passWordInput,text);
    }
  
  }

  module.exports=LoginPageClass;