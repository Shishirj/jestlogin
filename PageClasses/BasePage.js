const PageObject = require('puppeteer-page-object');
const CommonUtils = require('../Utils/CommonUtils');
const commonUtils = new CommonUtils();

class BasePage {
    constructor(){
        this.page=null;
    }

    async initPage(page) {
        this.page = page;
    }
      async waitForXpath(selector) {
        return await this.page.waitForXPath(selector);
      }
    
      async waitForSelector(selector) {
        return await this.page.waitForSelector(selector);
      }
    
      async clickElement(selector,message) {
          this.page.click(selector)
      }
  
      async clickElementUsingDocumentSelector(selector,message) {
          await this.page.evaluate(() => {
              document.querySelector(selector).click();
            });
      }

      async enterText(selector,text) {
        // this.page.waitFor(2000);
        // this.page.click(selector);
        // this.page.waitFor(2000);
        return await this.page.type(selector,text);
      }
}

module.exports=BasePage;