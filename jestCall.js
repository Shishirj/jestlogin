const { runCLI } = require('jest-cli');
const projectRootPath = 'E:/NewJestProject/jestlogin';
// Add any Jest configuration options here
const jestConfig = {
    roots: ['./TestScripts'],
    testRegex: '\\.spec\\.js$'
};
// Run the Jest asynchronously
const result= runCLI(jestConfig, [projectRootPath]);
// const start = (async function()  {
//     result = await runCLI(jestConfig, [projectRootPath]);
//     console.log(result);
// });
// start();

// Analyze the results
// (see typings for result format)
if (result.results.success) {
    console.log(`Tests completed`);
}
else {
    console.error(`Tests failed`);
}