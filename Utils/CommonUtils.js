import puppeteer from 'puppeteer';
class CommonUtils {

    constructor(page){
        this.page=null;
    }

    async init(page) {
        this.page = page;
    }

    async optimizeViewPort(page) {
        return page.setViewport({ width: 1366, height: 768})
      }
    
    async defaultTimeOut(page,value) {
          return page.setDefaultNavigationTimeout(value);
      }
}

module.exports=CommonUtils;