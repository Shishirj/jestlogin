const express = require("express");
const app = express();
const db = require("./db");
const pets = ["Cat","Dog","Rat","Monkey"];
const statusCode = [200,201,400,401,403,404];
const bodyParser = require("body-parser");
const petsRoutes = require("./routes/pets");
const statusRoutes = require("./routes/status");

app.use(bodyParser.json());
app.use("/pets", petsRoutes);
app.use("/status", statusRoutes);
app.use("/statusCode", statusRoutes);

app.get("/", (req, res) => {
  return res.json(pets);
});

app.get("/allstatuses", (req, res) => {
  return res.json(statusCode);
});

module.exports = app;
